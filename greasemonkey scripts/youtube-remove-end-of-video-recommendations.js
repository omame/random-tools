// ==UserScript==
// @name		YouTube remove recommendations
// @description	Removes YouTube recommendations at the end of videos
// @author		omame (from antoine-dh)
// @match        *://*.youtube.com/*
// @version		1.0
// ==/UserScript==

// from https://stackoverflow.com/a/14570614
const observeDOM = (function () {
	const MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

	return function (obj, callback) {
		if (!obj || !obj.nodeType === true) {
			return;
		}
		if (MutationObserver) {
			const obs = new MutationObserver(function (mutations) {
				if (mutations[0].addedNodes.length) {
					callback(mutations[0]);
                }
			});
			obs.observe(obj, {childList: true, subtree: true});
		} else if (window.addEventListener) {
			obj.addEventListener('DOMNodeInserted', callback, false);
		}
	}
})();

observeDOM(document, () => {
	for (let i of document.getElementsByClassName('ytp-ce-element')) { // removes all suggestions at the video end
		i.remove();
	}
});
