function get-nodes-by-nodepool {
    if [ $# -eq 0 ]
    then
        echo "Nodepool not supplied"
        return
    fi
    kubectl get nodes -l "karpenter.sh/nodepool=${1}"
}

function get-pods-by-nodepool {
    (
        echo "Pod Node NodePool"
        kubectl get pods --template '{{range .items}}{{.metadata.name}} {{.spec.nodeName}}{{"\n"}}{{end}}' | while read pod node
        do
            nodepool=$(kubectl get node $node -o jsonpath='{.metadata.labels.karpenter\.sh/nodepool}')
            echo "$pod $node $nodepool"
        done
    ) | column -t -s' '
}
