function ccd {
    if [[ -z "${GOPATH}" ]]; then
        echo "\$GOPATH is undefined"
        return
    fi
    if [[ $# -eq 0 ]]; then
        cd "${GOPATH}"
        return
    fi

    dirs=($(find "${GOPATH}/src" -type d -wholename "*${1}/.git" | sed 's/\.git//'))
    if [[ ${#dirs[@]} -eq 0 ]]; then
        echo "No repository found"
        return
    fi
    if [[ ${#dirs[@]} -eq 1 ]]; then
        cd "${dirs[0]}"
        return
    fi
    echo 'More than one directory detected:'
    for ((i=0; i<${#dirs[*]}; i++)); do
        echo "  $((i+1)). ${dirs[i]#${GOPATH}/src/}"
    done
    echo "Which one do you want? "
    read -r answer
    cd "${dirs[$((answer-1))]}"
}

_gl_options=$(find "${GOPATH}/src/" -type d -wholename "*${1}/.git" | awk -F / '{print $(NF-1)}')
complete -W "${_gl_options}" 'ccd'
