# ccd

Change directory to repositories stored in your `$GOPATH/src/` following the Go directory structure. This works great with [`gitclone`](https://gitlab.com/yakshaving.art/gitclone): you can checkout all your code into `$GOPATH/src/` and keep them tidy and easy accessible through `ccd`. Oh, and it supports bash tab expansion, too.

## Installation

Add the code to your bash profile and reload the shell.

## FAQ

### I don't code in Go. Can I use `ccd` anyway?

`ccd` uses the Go directory structure because it's tidy and sensible. You can use `ccd` anyway by setting a `GOPATH` variable.
